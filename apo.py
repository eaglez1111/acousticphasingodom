#EagleZ, tim.eagle.zhao@gmail.com

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal, integrate
import scipy.io.wavfile
import csv

SOUND_SPEED = 343 # meter/sec
SCALING_FACTOR = 1.0

def findPhShift(rcd,fs,fq):
    waveLen = (fs/fq)
    length = len(rcd)
    phShift = np.array([0.0])
    t = np.array([0.0])
    lastFeature = 0
    for i in range(1,length):
        if ( (rcd[i]>0) and (rcd[i-1]<0) ):
            t = np.append(t,i/fs)
            phShift = np.append(phShift,phShift[-1]+i-lastFeature-waveLen)
            lastFeature = i
    return (t,phShift)


if __name__ == "__main__":
  (fname,F,actualFs) = ('rcd09121543.wav',210,44100-18.38)
  bandwidth = 10

  nominalFs,rcd = scipy.io.wavfile.read('data/'+fname)
  B, A = scipy.signal.butter(3, ((F-bandwidth/2)/(actualFs/2), (F+bandwidth/2)/(actualFs/2)), 'bandpass', output='ba')
  rcd = scipy.signal.filtfilt(B, A, rcd)

  t,phShift = findPhShift(rcd,actualFs,F)
  dist = 1.0*phShift * SOUND_SPEED / actualFs * SCALING_FACTOR

  plt.plot(t,dist,'.')
  plt.title('Acoustic Position')
  plt.xlabel('Time [sec]')
  plt.ylabel('Position [meter]')
  plt.show()
